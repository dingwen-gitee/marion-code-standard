package com.marion.codestandard.demo008;

import com.marion.codestandard.demo004.BaseTest;
import org.junit.jupiter.api.Test;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import static org.junit.jupiter.api.Assertions.*;

class Demo008Test extends BaseTest {

    @Test
    void cloneUserDTO() throws RunnerException {
        OptionsBuilder builder = new OptionsBuilder();
        Options build = builder.include(Demo008.class.getSimpleName())
                .forks(1)
                .build();
        new Runner(build).run();
    }
}