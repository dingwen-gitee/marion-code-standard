package com.marion.codestandard.demo004;

import com.marion.codestandard.demo001.v2.User002;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Marion
 * @date 2022/5/12 16:28
 */
class Demo004Test extends BaseTest {

    @Autowired
    private Demo004 demo004;

    @Test
    void findById() {
        User002 user002 = demo004.findById();
        System.out.println(user002);
    }
}