package com.marion.codestandard.demo006.v1;

import com.marion.codestandard.demo006.Apple;
import com.marion.codestandard.demo006.ColorEnums;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Farm001Test {

    @Test
    void testFilterGreenApples() {
        // Setup
        final List<Apple> list = List.of(new Apple(0, "color", 0));
        final List<Apple> expectedResult = List.of(new Apple(0, "color", 0));

        // Run the test
        final List<Apple> result = Farm001.filterGreenApples(list);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    void testFilterRedApples() {
        // Setup
        final List<Apple> list = List.of(new Apple(0, "color", 0));
        final List<Apple> expectedResult = List.of(new Apple(0, "color", 0));

        // Run the test
        final List<Apple> result = Farm001.filterRedApples(list);

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    void testFilterRedApples1() {
        // Setup
        final List<Apple> list = List.of(new Apple(0, "color", 0));
        final List<Apple> expectedResult = List.of(new Apple(0, "color", 0));

        // Run the test
        final List<Apple> result = Farm001.filterRedApples(list, ColorEnums.GREEN);

        // Verify the results
        assertEquals(expectedResult, result);
    }
}
