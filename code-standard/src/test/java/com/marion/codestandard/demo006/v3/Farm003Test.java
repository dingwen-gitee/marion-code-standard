package com.marion.codestandard.demo006.v3;

import com.marion.codestandard.demo006.Apple;
import com.marion.codestandard.demo006.ColorEnums;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Marion
 * @date 2022/5/13 11:23
 */
class Farm003Test {

    @Test
    void filterApples() {

        List<Apple> apples = List.of(Apple.builder().id(1).color(ColorEnums.GREEN.name()).weight(100).build(),
                Apple.builder().id(1).color(ColorEnums.RED.name()).weight(200).build(),
                Apple.builder().id(1).color(ColorEnums.YELLOW.name()).weight(300).build());

        /**
         * v3: 筛选绿色
         */
        Farm003.filterApples(apples, apple -> apple.getColor().equals(ColorEnums.GREEN.name()));
        Farm003.filterApples(apples, apple -> apple.getColor().equals(ColorEnums.GREEN.name()) && apple.getWeight() > 100);

    }
}