package com.marion.codestandard.demo010;

import com.marion.codestandard.demo001.v2.User002;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserControllerTest {

    private UserController userControllerUnderTest;

    @BeforeEach
    void setUp() {
        userControllerUnderTest = new UserController();
    }

    @Test
    void testUser() {
        // Setup
        final User002 expectedResult = new User002(0, "name", "email", "password");

        // Run the test
        final User002 result = userControllerUnderTest.user();

        // Verify the results
        assertEquals(expectedResult, result);
    }
}
