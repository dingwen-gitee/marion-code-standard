package com.marion.codestandard.demo009.rpc;

import com.marion.codestandard.demo004.BaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.ExecutionException;

/**
 * @author Marion
 * @date 2022/5/13 17:46
 */
class ShopServiceTest extends BaseTest {

    @Autowired
    private ShopService shopService;

    @Test
    void goods() {
        shopService.goods();
    }

    @Test
    void syncGoods() {
        try {
            shopService.syncGoods();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}