package com.marion.codestandard.demo010;

import com.marion.codestandard.demo001.v2.User002;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Marion
 * @date 2022/5/13 18:26
 */
@RestController
@RequestMapping("/")
public class UserController {

    @GetMapping("/user")
    public User002 user() {
        return User002.builder().id(1)
                .name("1")
                .build();
    }

}
