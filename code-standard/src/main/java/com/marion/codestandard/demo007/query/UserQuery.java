package com.marion.codestandard.demo007.query;

import com.marion.codestandard.common.constraints.Phone;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * @author Marion
 * @date 2022/5/13 14:38
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserQuery {

    private Long id;

    @NotBlank
    @Length(min = 4, max = 10)
    private String name;

    @NotBlank
    @Email
    private String email;

    /**
     * ^((13[0-9])|(15[^4,\\D])|(18[0,3-9]))\\d{8}$
     */
    @NotBlank
    //@Pattern(regexp = "^((13[0-9])|(15[^4,\\D])|(18[0,3-9]))\\d{8}$", message = "手机号格式不正确")
    @Phone
    private String phone;

    @Min(value = 18)
    @Max(value = 200)
    private int age;

    @NotBlank
    @Length(min = 4, max = 12, message = "昵称4-12位")
    private String nickname;

}
