package com.marion.codestandard.demo007;

import com.marion.codestandard.demo007.query.UserQuery;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 6. 行为参数化传递代码
 * 就农场库存程序而言，你必须实现一个从列表中筛选绿苹果的功能
 * @author Marion
 * @date 2022/5/13 17:17
 */
@RestController
@RequestMapping("/")
public class Demo007 {

    /**
     * 保存User信息
     */
    @PostMapping("/user/save")
    public Object saveUser(@Validated @RequestBody UserQuery user) {
        System.out.println(user);
        return user;
    }

}
