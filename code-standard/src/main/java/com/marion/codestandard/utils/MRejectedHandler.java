package com.marion.codestandard.utils;

/**
 * @author Marion
 * @date 2022/5/30 17:06
 */
public interface MRejectedHandler {

    void rejectedHandler(Runnable runnable, MThreadPoolExecutor mThreadPoolExecutor);

}
