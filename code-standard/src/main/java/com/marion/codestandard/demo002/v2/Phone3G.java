package com.marion.codestandard.demo002.v2;

/**
 * @author Marion
 * @date 2022/5/13 16:06
 */
public class Phone3G extends AbstractPhone {
    /**
     * 拨通电话
     */
    @Override
    public void dial() {
        System.out.println("1. [Phone3G] dial");
    }

    /**
     * 关闭通话
     */
    @Override
    public void hangup() {
        System.out.println("3. [Phone3G] hangup");
    }

    @Override
    public void chat() {
        System.out.println("2. [Phone3G] chat");
    }
}
