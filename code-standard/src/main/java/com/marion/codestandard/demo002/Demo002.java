package com.marion.codestandard.demo002;

import com.marion.codestandard.demo002.v1.Phone001;
import com.marion.codestandard.demo002.v1.Phone002;
import com.marion.codestandard.demo002.v1.Phone003;
import com.marion.codestandard.demo002.v2.Phone2G;
import com.marion.codestandard.demo002.v2.Phone3G;
import com.marion.codestandard.demo002.v2.Phone4G;

/**
 * 2. 让接口职责单一
 * @author Marion
 * @date 2022/5/13 15:50
 */
public class Demo002 {

    /**
     * 一次电话通信包含四个过程：拨号、通话、回应、挂机，我们来思考一下该如何划分职责，
     * 这四个过程包含了两个职责：一个是拨号和挂机表示的是通信协议的链接和关闭，
     * 另外一个是通话和回应所表示的数据交互。
     */
    public static void main(String[] args) {

        /**
         * v1:
         * 1. 很多重复代码，难以维护和扩展
         * 2. 面向过程开发
         */
        Phone001 phone001 = new Phone001();
        phone001.start();

        Phone002 phone002 = new Phone002();
        phone002.start();

        Phone003 phone003 = new Phone003();
        phone003.start();

        /**
         * v2:
         * 1. 扩展方便，结构清晰
         */
        Phone2G phone2G = new Phone2G();
        phone2G.start();
        Phone3G phone3G = new Phone3G();
        phone3G.start();
        Phone4G phone4G = new Phone4G();
        phone4G.start();

    }


}
