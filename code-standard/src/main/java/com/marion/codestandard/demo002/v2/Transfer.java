package com.marion.codestandard.demo002.v2;

/**
 * 传输协议
 * @author Marion
 * @date 2022/5/13 16:06
 */
public interface Transfer {

    /**
     * 聊天
     */
    public void chat();

}
