package com.marion.codestandard.demo002.v2;

/**
 * @author Marion
 * @date 2022/5/13 16:10
 */
public interface Phone {

    public void start();

}
