package com.marion.codestandard.demo002.v2;

/**
 * 通信协议（2G，3G，4G）
 * @author Marion
 * @date 2022/5/13 16:04
 */
public interface Connection {

    /**
     * 拨通电话
     */
    public void dial();

    /**
     * 关闭通话
     */
    public void hangup();

}
