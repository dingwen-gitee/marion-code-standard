package com.marion.codestandard.demo002.v2;

/**
 * 2G手机
 * @author Marion
 * @date 2022/5/13 16:06
 */
public class Phone2G extends AbstractPhone {
    /**
     * 拨通电话
     */
    @Override
    public void dial() {
        System.out.println("1. [Phone2G] dial");
    }

    /**
     * 关闭通话
     */
    @Override
    public void hangup() {
        System.out.println("3. [Phone2G] hangup");
    }

    @Override
    public void chat() {
        System.out.println("2. [Phone2G] chat");
    }
}
