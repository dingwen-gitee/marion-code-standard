package com.marion.codestandard.demo002.v2;

/**
 * @author Marion
 * @date 2022/5/13 16:10
 */
public abstract class AbstractPhone implements Connection, Transfer, Phone {

    @Override
    public void start() {
        dial();
        chat();
        hangup();
    }
}
