package com.marion.codestandard.demo002.v1;

/**
 * 2G手机
 * @author Marion
 * @date 2022/5/13 15:55
 */
public class Phone001 {

    public void start() {
        dial();
        chat();
        resp();
        hangup();
    }

    public void dial() {
        System.out.println("1. [2G] dialog");
    }

    public void chat() {
        System.out.println("2. [2G] chat");
    }

    public void resp() {
        System.out.println("3. [2G] resp");
    }

    public void hangup() {
        System.out.println("4. [2G] hangup");
    }

}
