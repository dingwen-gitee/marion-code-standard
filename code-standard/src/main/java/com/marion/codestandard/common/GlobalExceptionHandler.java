package com.marion.codestandard.common;

import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author Marion
 * @date 2022/5/13 14:47
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public AppResponse validationException(MethodArgumentNotValidException e) {
        System.out.println(e.getMessage());
        FieldError fieldError = e.getBindingResult().getFieldError();
        return AppResponse.builder()
                .code(400)
                .message(fieldError.getDefaultMessage())
                .build();
    }

}
