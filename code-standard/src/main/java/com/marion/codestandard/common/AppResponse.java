package com.marion.codestandard.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Marion
 * @date 2022/5/13 14:55
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AppResponse<T> {

    private int code;
    private String message;
    private T data;

}
