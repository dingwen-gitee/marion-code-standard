package com.marion.codestandard.demo001.v1;

/**
 * @author Marion
 * @date 2022/5/13 15:36
 */
public class User001 {

    private int id;

    private String name;

    private String email;

    private String password;

    public User001() {
    }

    public User001(int id, String name, String email, String password) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * com.marion.codestandard.demo001.v1.User001@26ba2a48
     * User002(id=1, name=marion, email=email, password=123456)
     */
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
