package com.marion.codestandard.demo001;

import com.marion.codestandard.demo001.v1.User001;
import com.marion.codestandard.demo001.v2.User002;

/**
 * 1. 创建对象使用@Builder
 * @author Marion
 * @date 2022/5/13 15:39
 */
public class Demo001 {

    public static void main(String[] args) {

        User001 user001 = new User001();
        user001.setId(1);
        user001.setName("marion");

        System.out.println(user001);

        User002 user002 = User002.builder()
                .id(1)
                .name("marion")
                .build();

        System.out.println(user002);

    }

}
