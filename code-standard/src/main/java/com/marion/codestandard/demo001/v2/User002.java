package com.marion.codestandard.demo001.v2;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Marion
 * @date 2022/5/13 15:36
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User002 {

    private int id;

    private String name;

    private String email;

    private String password;

}
