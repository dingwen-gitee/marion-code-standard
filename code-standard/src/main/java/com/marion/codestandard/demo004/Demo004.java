package com.marion.codestandard.demo004;

import com.marion.codestandard.demo001.v2.User002;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 4. 依赖抽象而不是实现
 * @author Marion
 * @date 2022/5/13 16:15
 */
@Service
public class Demo004 {

    @Autowired
    private UserService userService;

    public User002 findById() {
        return userService.findById(1).orElseThrow();
    }

}
