package com.marion.codestandard.demo004.impl;

import com.marion.codestandard.demo001.v2.User002;
import com.marion.codestandard.demo004.UserService;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Marion
 * @date 2022/5/13 16:25
 */
@Service
public class UserServiceImpl implements UserService {
    @Override
    public Optional<User002> findById(int id) {
        return Optional.ofNullable(User002.builder()
                .id(3)
                .name("demo004")
                .build());
    }
}
