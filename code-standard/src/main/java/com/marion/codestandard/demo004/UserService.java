package com.marion.codestandard.demo004;

import com.marion.codestandard.demo001.v2.User002;

import java.util.Optional;

/**
 * @author Marion
 * @date 2022/5/13 16:24
 */
public interface UserService {

   Optional<User002> findById(int id);

}
