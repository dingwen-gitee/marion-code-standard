package com.marion.codestandard.demo012;

import com.marion.codestandard.demo008.dto.UserDTO;

import java.util.List;

public class Demo012 {

    public UserDTO findById(int id) {
        return UserDTO.builder()
                .id(1)
                .name("marion")
                .build();
    }

    public List<UserDTO> findAll() {
        return List.of(UserDTO.builder()
                .id(1)
                .name("marion")
                .build());
    }

}
