package com.marion.codestandard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeStandardApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeStandardApplication.class, args);
    }

}
