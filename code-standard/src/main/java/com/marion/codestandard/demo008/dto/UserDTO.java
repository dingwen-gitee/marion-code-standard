package com.marion.codestandard.demo008.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Marion
 * @date 2022/5/13 16:16
 */
@Data
@NoArgsConstructor
@Builder
public class UserDTO implements Cloneable {

    public UserDTO(int id, String name) {
        if (name.length() != 0) {
            name = name.substring(0, 1);
        }
        this.id = id;
        this.name = name;
    }

    private int id;

    private String name;

    @Override
    public Object clone() {
        UserDTO userDTO = null;
        try {
            userDTO = (UserDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return userDTO;
    }

}
