package com.marion.codestandard.demo008.benchmark;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

/**
 * @author Marion
 * @date 2022/5/13 16:36
 */
public class JMHSample_01_HelloWorld {
    @Benchmark
    @BenchmarkMode(Mode.Throughput)
    @Warmup(iterations = 3)
    @Measurement(iterations = 3, time = 5, timeUnit = TimeUnit.SECONDS)
    @Threads(1)
    @Fork(1)
    @OutputTimeUnit(TimeUnit.SECONDS)
    public void wellHelloThere() throws InterruptedException {
        Thread.sleep(1000);
    }

    /**
     * JMH的基础配置
     * include:benchmark所在类的名字，可以使用正则表达
     * warmupIteration:预热的迭代次数，这里为什么要预热的原因是由于JIT的存在，随着代码的运行，会动态对代码的运行进行优化。因此在测试过程中需要先预热几轮，让代码运行稳定后再实际进行测试
     * measurementIterations:实际测试轮次
     * output:测试报告输出位置,不配置则输出到控制台。
     */
    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(JMHSample_01_HelloWorld.class.getSimpleName())
                .forks(1)
                .build();
        new Runner(opt).run();

    }
}

