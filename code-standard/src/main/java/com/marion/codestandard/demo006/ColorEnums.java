package com.marion.codestandard.demo006;

/**
 * @author Marion
 * @date 2022/5/13 10:49
 */
public enum ColorEnums {

    GREEN,
    YELLOW,
    RED;

}
