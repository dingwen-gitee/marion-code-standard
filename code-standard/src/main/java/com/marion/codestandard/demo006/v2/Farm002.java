package com.marion.codestandard.demo006.v2;

import com.marion.codestandard.demo006.Apple;
import com.marion.codestandard.demo006.ColorEnums;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marion
 * @date 2022/5/13 18:08
 */
public class Farm002 {

    /**
     * v2: 筛选重量苹果
     * @param flag true-比较颜色 false-比较重量
     */
    public static List<Apple> filterGreenApples(List<Apple> list, ColorEnums color, int wight, boolean flag) {
        List<Apple> ret = new ArrayList<>();
        for (Apple apple : list) {
            if (flag) {
                if (apple.getColor().equals(color.name())) {
                    ret.add(apple);
                }
            } else {
                if (apple.getWeight() > wight) {
                    ret.add(apple);
                }
            }
        }
        return ret;
    }

}
