package com.marion.codestandard.demo006.v4;

import com.marion.codestandard.demo006.Apple;
import com.marion.codestandard.demo006.ColorEnums;
import com.marion.codestandard.demo006.v3.ApplePredicate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 谓词建模
 * @author Marion
 * @date 2022/5/13 18:08
 */
public class Farm004 {

    /**
     * v4: 筛选重量苹果
     */
    public static List<Apple> filterApples(List<Apple> list, ColorEnums color) {
        return list.stream()
                .filter(v -> v.getColor().equals(color.name()))
                .collect(Collectors.toList());
    }

}
