package com.marion.codestandard.demo006;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Marion
 * @date 2022/5/13 18:16
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Apple {

    private int id;

    private String color;

    private int weight;

}
