package com.marion.codestandard.demo006.v3;

import com.marion.codestandard.demo006.Apple;
import com.marion.codestandard.demo006.ColorEnums;

import java.util.ArrayList;
import java.util.List;

/**
 * 谓词建模
 * @author Marion
 * @date 2022/5/13 18:08
 */
public class Farm003 {

    /**
     * v3: 筛选重量苹果
     */
    public static List<Apple> filterApples(List<Apple> list, ApplePredicate<Apple> predicate) {
        List<Apple> ret = new ArrayList<>();
        for (Apple apple : list) {
            if (predicate.filter(apple)) {
                ret.add(apple);
            }
        }
        return ret;
    }

}
