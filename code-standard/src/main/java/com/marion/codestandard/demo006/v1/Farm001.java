package com.marion.codestandard.demo006.v1;

import com.marion.codestandard.demo006.Apple;
import com.marion.codestandard.demo006.ColorEnums;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marion
 * @date 2022/5/13 18:08
 */
public class Farm001 {

    /**
     * v1. 筛选绿色苹果
     */
    public static List<Apple> filterGreenApples(List<Apple> list) {
        List<Apple> ret = new ArrayList<>();
        for (Apple apple : list) {
            if (apple.getColor().equals(ColorEnums.GREEN.name())) {
                ret.add(apple);
            }
        }
        return ret;
    }

    /**
     * v1. 筛选红色苹果
     */
    public static List<Apple> filterRedApples(List<Apple> list) {
        List<Apple> ret = new ArrayList<>();
        for (Apple apple : list) {
            if (apple.getColor().equals(ColorEnums.RED.name())) {
                ret.add(apple);
            }
        }
        return ret;
    }

    /**
     * v2. 颜色作为参数
     */
    public static List<Apple> filterRedApples(List<Apple> list, ColorEnums color) {
        List<Apple> ret = new ArrayList<>();
        for (Apple apple : list) {
            if (apple.getColor().equals(color.name())) {
                ret.add(apple);
            }
        }
        return ret;
    }

}
