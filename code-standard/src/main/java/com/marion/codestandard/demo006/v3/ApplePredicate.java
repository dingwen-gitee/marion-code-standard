package com.marion.codestandard.demo006.v3;

/**
 * @author Marion
 * @date 2022/5/13 11:11
 */
@FunctionalInterface
public interface ApplePredicate<T> {

    boolean filter(T apple);

}
