package com.marion.codestandard.demo003;

import com.marion.codestandard.demo002.v2.Phone;
import com.marion.codestandard.demo002.v2.Phone2G;
import com.marion.codestandard.demo002.v2.Phone3G;
import com.marion.codestandard.demo002.v2.Phone4G;

/**
 * 3. 增强类的可替换性
 * @author Marion
 * @date 2022/5/13 16:15
 */
public class Demo003 {

    public static void main(String[] args) {

        Phone phone2G = new Phone2G();
        phoneCall(phone2G);

        Phone phone3G = new Phone3G();
        phoneCall(phone3G);

        Phone phone4G = new Phone4G();
        phoneCall(phone4G);

    }

    public static void phoneCall(Phone phone) {
        phone.start();
    }


}
