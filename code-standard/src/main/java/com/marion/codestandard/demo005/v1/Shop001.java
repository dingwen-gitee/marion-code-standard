package com.marion.codestandard.demo005.v1;

import com.marion.codestandard.demo005.enums.PaymentType;

/**
 * 商城购买商品，选择支付方式：微信支付、支付宝支付、银行卡支付、白条
 * @author Marion
 * @date 2022/5/13 16:33
 */
public class Shop001 {

    public void buyGoods(PaymentType paymentType) {

        if (paymentType == PaymentType.ALIPAY) {
            alipayBuyGoods();
        } else if(paymentType == PaymentType.WEXIN) {
            wxBuyGoods();
        } else if(paymentType == PaymentType.BANKCARD) {
            bankcardBuyGoods();
        } else if (paymentType == PaymentType.WHITE) {
            whiteBuyGoods();
        }

    }

    public void alipayBuyGoods() {
        System.out.println("[buyGoods] alipayBuyGoods");
    }

    public void wxBuyGoods() {
        System.out.println("[buyGoods] wxBuyGoods");

    }

    public void bankcardBuyGoods() {
        System.out.println("[buyGoods] bankcardBuyGoods");
    }

    public void whiteBuyGoods() {
        System.out.println("[buyGoods] whiteBuyGoods");
    }

}
