package com.marion.codestandard.demo005;

import com.marion.codestandard.demo005.enums.PaymentType;
import com.marion.codestandard.demo005.v1.Shop001;
import com.marion.codestandard.demo005.v2.GoodsContext;
import com.marion.codestandard.demo005.v2.Payment;
import com.marion.codestandard.demo005.v2.strategy.AliPaymentStrategy;

/**
 * 5. 策略模式优化if-else
 * @author Marion
 * @date 2022/5/13 16:15
 */
public class Demo005 {

    public static void main(String[] args) {

        /**
         * v1:
         * 1. 代码结构复杂，扩展安全性低
         */
        Shop001 shop001 = new Shop001();
        shop001.buyGoods(PaymentType.WEXIN);
        shop001.buyGoods(PaymentType.ALIPAY);
        shop001.buyGoods(PaymentType.BANKCARD);
        shop001.buyGoods(PaymentType.WEXIN);

        /**
         * v2:
         * 1. 新增支付类型只需要新增类与实现
         */
        Payment payment = GoodsContext.getGoodsContext(PaymentType.WEXIN);
        payment.buy();
        Payment payment1 = GoodsContext.getGoodsContext(PaymentType.ALIPAY);
        payment1.buy();
        Payment payment2 = GoodsContext.getGoodsContext(PaymentType.BANKCARD);
        payment2.buy();
        GoodsContext goodsContext = new GoodsContext(new AliPaymentStrategy());
        goodsContext.buy();

    }

}
