package com.marion.codestandard.demo005.v2;

/**
 * @author Marion
 * @date 2022/5/13 16:47
 */
public interface Payment {

    public void buy();

}
