package com.marion.codestandard.demo005.v2;

import com.marion.codestandard.demo005.enums.PaymentType;
import com.marion.codestandard.demo005.v2.strategy.AliPaymentStrategy;
import com.marion.codestandard.demo005.v2.strategy.BankCardPaymentStrategy;
import com.marion.codestandard.demo005.v2.strategy.WxPaymentStrategy;

/**
 * @author Marion
 * @date 2022/5/13 16:46
 */
public class GoodsContext {

    private Payment payment;

    public GoodsContext(Payment payment) {
        this.payment = payment;
    }

    public void buy() {
        payment.buy();
    }

    public static Payment getGoodsContext(PaymentType type) {
        Payment payment = null;
        switch (type) {
            case WEXIN:
                payment = new WxPaymentStrategy();
                break;
            case ALIPAY:
                payment = new AliPaymentStrategy();
                break;
            case BANKCARD:
                payment = new BankCardPaymentStrategy();
                break;
            default:
                break;
        }
        return payment;
    }
}
