package com.marion.codestandard.demo005.v2;

import com.marion.codestandard.demo005.enums.PaymentType;

/**
 * @author Marion
 * @date 2022/5/13 16:41
 */
public class Shop002 {

    public void buyGoods(PaymentType paymentType) {

        if (paymentType == PaymentType.ALIPAY) {
            System.out.println("[buyGoods] " + paymentType);
        } else if(paymentType == PaymentType.WEXIN) {
            System.out.println("[buyGoods] " + paymentType);
        } else if(paymentType == PaymentType.BANKCARD) {
            System.out.println("[buyGoods] " + paymentType);
        } else if (paymentType == PaymentType.WHITE) {
            System.out.println("[buyGoods] " + paymentType);
        }

    }

}
