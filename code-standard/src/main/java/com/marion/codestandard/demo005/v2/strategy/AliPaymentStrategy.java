package com.marion.codestandard.demo005.v2.strategy;

import com.marion.codestandard.demo005.v2.Payment;

/**
 * @author Marion
 * @date 2022/5/13 16:47
 */
public class AliPaymentStrategy implements Payment {
    @Override
    public void buy() {
        System.out.println("[AliPamentStrategy]");
    }
}
