package com.marion.codestandard.demo005.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 支付方式
 * @author Marion
 * @date 2022/5/13 16:35
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum PaymentType {

    WEXIN(1, "微信支付"),
    ALIPAY(2, "支付宝支付"),
    BANKCARD(3, "银行卡支付"),
    WHITE(4, "白条"),
    ;

    private int id;

    private String desc;

}
