## 我的社交资料
- B站UP账号：[后端研发Marion](https://space.bilibili.com/269097482)
- 今日头条账号：[后端研发Marion](https://www.toutiao.com/c/user/token/MS4wLjABAAAAChrLQhHvIVE31-TLHrkth8_9uQLhosRHQmKC5jkat70/)
- CSDN账号：[后台研发Marion](https://blog.csdn.net/luomao2012)
- 微信公众号：【后端研发Marion】加微信进JAVA技术交流群

|                       微信公众号                      |                      微信群（备注：加群）                       |
| :----------------------------------------------: | :-----------------------------------------------: |
| <img src="./resources/images/qrcode.jpg" width="200" /> | <img src="./resources/images/wx.jpg" width="200" /> |


# 代码规范与性能优化的100个建议

## 1. 创建对象使用@Builder

## 2. 让接口职责单一

### 一次电话通信包含四个过程：拨号、通话、回应、挂机

## 3. 增强类的可替换性

### 所有引用基类的地方必须能透明地使用其子类的对象

## 4. 依赖抽象而不是实现

在面向过程开发中，我们考虑的是如何实现，依赖的是每个具体实现，而在OOP中，则需要依赖每个接口，而不能依赖具体的实现，比如我们要到北京出差，应该依赖交通工具，而不是依赖的具体飞机或火车，也就是说我们依赖的是交通工具的运输能力，而不是具体的一架飞机或某一列火车。这样的依赖可以让我们实现解耦，保持代码间的松耦合，提高代码的复用率，这也是依赖倒置原则（Dependence Inversion Principle，简称DIP）提出的要求。


## 5. 策略模式优化if-else

## 6. 行为参数化传递代码

*XMind - Trial Version*